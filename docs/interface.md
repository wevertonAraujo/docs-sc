# Página de Alteração da Interface de Rede

Iremos fazer a configuração do endereço de rede da máquina **_canoas_** (server).

Antes de tudo vamos verificar os endereços das interfaces, para isso utiliza-se o comando abaixo:

    $ ip -br -c addr

Após a verificação dos endereços iremos alterar o arquivo de configuração das interafce de Redes. Que esse arquivo fica armazenado no diretório **`/etc/netplan/`**.
Esse arquivo tem sua nomeclatura assim **`50-cloud-init.yaml`**.
Iremos edita-lo com o editor nano a seguir:

    $ nano /etc/netplan/50-cloud-init.yaml

Colocaremos os seguintes dados:

- IP: 10.172.113.92/24
- Gateway: 10.172.113.1
- DNS: 8.8.8.8 ou 1.1.1.1

Após a edição do arquivo teremos que aplicar as configurações inserinas, mas antes vamos verificar se não exite erro na hora da digitação e em seguida aplicaremos as configurações com os comandos a seguir:

    $ netplan try
    $ netplan apply 

Após isso vamos novamente verificar se o endereço de Rede foi alterado com o comando:

    $ ip -br -c addr