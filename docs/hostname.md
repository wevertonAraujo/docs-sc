# Página de Alteração do nome da Máquina (Servidor)

Para a verificação do nome da máquina utilizamos os comandos abaixo:

	$ hostname  
	$ hostnamectl 

Já para a mudança do nome da máquina iremos editar os conteúdos dos arquivos com o editor nano abaixo:

	$ nano /etc/hosts
	$ nano /etc/hostname

Após a edição dos arquivos iremos reiniciar a máquina para aplicar as configurações com o comando abaixo:

	$ reboot

OBS: O nome da máquina (servidor) será **_canoas_** pois é o 
principal rio de Santa Catarina.
